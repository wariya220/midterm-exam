import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';

void main() {
  runApp(UiREG());
}

class UiREG extends StatefulWidget {
  @override
  State<UiREG> createState() => _UiREGState();
}

class _UiREGState extends State<UiREG> {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        home: Scaffold(
          appBar: appBarMain(),
          body: bodyMain(),
          drawer: drawerMAin(),
        ),
      ),
    );
  }
}

bodyMain() {
  return ListView(
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
              margin: const EdgeInsets.only(top: 1, bottom: 8),
              child: Theme(
                  data: ThemeData(
                    iconTheme: IconThemeData(
                      color: Colors.green,
                    ),
                  ),
                  child: profileActionItem())),
          Divider(
            color: Colors.white10,
          ),
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 200,
            child: Image.network(
              "https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png",
              fit: BoxFit.contain,
            ),
          ),
          Container(
            color: Colors.white10,
            height: 68,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "หน้าหลัก",
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 500,
            child: Image.network(
              "https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.6435-9/106477843_3195298757175320_7926430920123409583_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=730e14&_nc_ohc=F8FaP3LQXmAAX98uA9b&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfAHhqEROQHdIM1fSqOUJWKDjr-gLTYc9sA9MBVhazbZcQ&oe=63FAFF33",
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
    ],
  );
}

Widget profileActionItem() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      mainButton(),
      registerButton(),
      resultsButton(),
      scheduleButton(),
      recordButton(),
      //buildPayButton(),
    ],
  );
}

Widget mainButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.home,
          //color: Colors.red.shade800,
        ),
        onPressed: () {},
      ),
      Text("หน้าหลัก"),
    ],
  );
}

Widget registerButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.app_registration,
          //color: Colors.red.shade800,
        ),
        onPressed: () {},
      ),
      Text("ลงทะเบียน"),
    ],
  );
}

Widget resultsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.app_registration_rounded,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("ผลการลงทะเบียน"),
    ],
  );
}

Widget scheduleButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.schedule,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("ตางรางเรียน"),
    ],
  );
}

Widget recordButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.record_voice_over_sharp,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("ประวัตินิสิต"),
    ],
  );
}

appBarMain() {
  return AppBar(
    backgroundColor: Colors.grey.shade500,
    title: Text("Reg BUU"),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.login_outlined),
        //color: Colors.black,
        onPressed: () {},
      ),
    ],
  );
}

drawerMAin() {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        const UserAccountsDrawerHeader(
          decoration: BoxDecoration(color: Color.fromARGB(255, 158, 158, 157)),
          accountName: Text(
            "Wariya Saichana",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          accountEmail: Text(
            "63160220@go.buu.ac.th",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          currentAccountPicture: Icon(Icons.person_3),
        ),
        ListTile(
          title: const Text('หน้าหลัก'),
          onTap: () {},
        ),
        ListTile(
          title: const Text('ลงทะเบียน'),
          onTap: () {},
        ),
        ListTile(
          title: const Text('ผลการลงทะเบียน'),
          onTap: () {},
        ),
        ListTile(
          title: const Text('ผลอนุมัติเพิ่ม-ลด'),
          onTap: () {},
        ),
        ListTile(
          title: const Text('ตารางเรียน/สอบ'),
          onTap: () {},
        ),
        ListTile(
          title: const Text('ประวัตินิสิต'),
          onTap: () {},
        ),
        ListTile(
          title: const Text('ผลการศึกษา'),
          onTap: () {},
        ),
        ListTile(
          title: const Text('ตั้งค่า'),
          onTap: () {},
        ),
      ],
    ),
  );
}
